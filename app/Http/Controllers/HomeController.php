<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Twitte;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function myTwitts()
    {
        $twittes = Twitte::where('user_id', '=',  Auth::user()->id)->get()->sortByDesc('created_at');
        return view('myposts', compact('twittes'));
    }
    public function allTwitts()
    {
        $twittes = Twitte::all()->sortByDesc('created_at');
        return view('home', compact('twittes'));
    }
    public function addtwitte()
    {
        return view('addtwitte');
    }
    public function createtwitte(Request $request) 
    {
        $twitte= New Twitte;
        $twitte->user_id = Auth::user()->id;
        $twitte->twitte = $request->twitte;

     if($twitte->save()){
            return redirect()->to('/all-twitts');
        }else{
            return redirect()->back();    
        }
    }
    public function edit($id)
    {
        $twitt= Twitte::find($id);

        return view('edittwitt',compact('twitt'));
    }
    public function update(Request $request,$id)
    {
        $twitt= Twitte::find($id);
        $twitt->twitte = $request->twitte;
        $twitt->save();
        return redirect('/my-twitts')->with('success','تم التعديل بنجاح');

    }
    public function delete($id)
    {
        Twitte::destroy($id);

        return redirect()->back()->with('success','تم الحذف بنجاح');

    }
}
