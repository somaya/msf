@extends('layouts.app')

@section('content')

        <div class="page-header">
        <div class="container">
            <div class="title-box">
                <h1 class="title"> الشكاوى والاقتراحات </h1>
                <div class="breadcrumb">
                    <span>
                        <a title="Homepage" href="/"><i class="ti ti-home"></i>&nbsp;&nbsp;الرئيسية</a>
                    </span>
                    <span class="bread-sep">&nbsp; | &nbsp;</span>
                    <span>الشكاوى والاقتراحات </span>
                </div>
            </div>
        </div>

    </div>

    <!-- Start Contact Section -->
    <section id="contact" class="section-block" data-scroll-index="8">
        <div class="bubbles-animate">
            <div class="bubble b_one"></div>
            <div class="bubble b_two"></div>
            <div class="bubble b_three"></div>
            <div class="bubble b_four"></div>
            <div class="bubble b_five"></div>
            <div class="bubble b_six"></div>
        </div>
        <div class="container">
            <div class="row">
                <!-- Start Contact Information -->
                <div class="col-md-5">
                    <div class="section-header-style2">
                        <h2>ستجدنا هنا</h2>
                        <p>
                المملكة العربيه السعودية - الدمام </p>
                    </div>
                    <div class="contact-details">




                        <!-- Start Contact Block -->
                        <div class="contact-block">
                            <h4>التواصل  </h4>
                            <div class="contact-block-side">
                                <i class="flaticon-paper-plane-1"></i>
                                <p>
                                    <span> info@msf.com</span>
                                    <span>00966530216596</span>
                                </p>
                            </div>
                        </div>
                        <!-- End Contact Block -->
                    </div>
                </div>
                <!-- End Contact Information -->

                <!-- Start Contact form Area -->
                <div class="col-md-7">

                    <div class="contact-form-block">
                        <div class="section-header-style2">
                            <h2>اكتب الشكوى او المقترح </h2>
                            
                        </div>
                        <form class="contact-form" method="POST" action="{{ url('/sugcom') }}">
                        @csrf
                            <select name="type" class="form-control" >
                                <option value="1">شكوى</option>
                                <option value="0">مقترح</option>
                            </select>
                            <textarea class="form-control" placeholder="الرسالة" name="massege"></textarea>
                            <button class="btn theme-btn">ارسل الرسالة</button>
                        </form>
                    </div>

                </div>
                <!-- End Contact form Area -->
            </div>
        </div>
    </section>
    <!-- End Contact Section -->

    <section class="contact-map">
        <div class="container">
            <div id="google-map"></div>
        </div>
    </section>
@endsection