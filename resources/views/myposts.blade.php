@extends('layouts.app')

@section('content')



    <div class="page-header">
        <div class="container">
            <div class="title-box">
                <h1 class="title">منشوراتي </h1>
                <div class="breadcrumb">
                    <span>
                        <a title="Homepage" href="/"><i class="ti ti-home"></i>&nbsp;&nbsp;الرئيسية</a>
                    </span>

                    <span class="bread-sep">&nbsp; | &nbsp;</span>
                    <span> منشوراتي</span>
                </div>
            </div>
        </div>

    </div>
    @include('message')

    <section class="section-block reviews-page">
        <div class="container">

            <div class="row">
                @foreach ($twittes as $twitte)
                    <div class="col-md-6">
                        <div class="review-item">
                            <div class="review-head">
                                <div class="review-item-img">
                                    <img src="/images/author.jpeg" class="img-fluid" alt="IMG" />
                                </div>
                                <div class="review-author">
                                    <h5>ِ{{$twitte->user->name }}</h5>

                                </div>
                            </div>
                            <div class="review-content">
                                <p> {{ $twitte->twitte}}</p>
                                <div class="rating">
                                    <a href="/twitt/{{$twitte->id}}/delete">حذف </a>
                                    <a href="/twitt/{{$twitte->id}}/edit">تعديل </a>

                                </div>

                            </div>
                        </div>

                    </div>
                @endforeach
            </div>

        </div>
    </section>

@endsection
