<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>عائلة التصلب اللويحي
    </title>

    <link rel="icon" href="favicon.png">
    <!-- Font Icons -->
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Animation -->
    <link rel="stylesheet" href="{{ asset('css/animate.min.cs') }}s">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- Light Case -->
    <link rel="stylesheet" href="{{ asset('css/lightcase.min.css" type="text/css') }}">
    <!-- Template style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<header class="header-area" id="header-area">
    <nav class="navbar navbar-expand-md fixed-top">
        <div class="container">
            <div class="site-logo"><a class="navbar-brand" href="{{ url('/')}}"><img src="images/MSF2.png"
                                                                                     class="img-fluid" alt="Img" /></a></div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="ti-menu"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav">
                    <li class="nav-item"><a href="{{ url('/') }}" data-scroll-nav="1">الرئيسية</a></li>
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle" href="#" id="dropdownMenuButton2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">الاقسام</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                            <a class="dropdown-item" href="{{ url('/doctors') }}">منشورات الاطباء </a>
                            <a class="dropdown-item" href="/all-twitts">منشورات العائلة</a>
                        </div>
                    </li>
                    <li class="nav-item"><a href="/all-twitts" data-scroll-nav="1">المنشورات </a></li>

                    @auth

                        <li class="nav-item"><a href="/my-twitts" data-scroll-nav="1">منشوراتي </a></li>
                        <li class="nav-item"><a href="/addtwitte" data-scroll-nav="2">إضافة منشور </a></li>
                        <li class="nav-item"><a href="{{ url('/sugcom') }}" data-scroll-nav="1">الشكاوى والاقتراحات</a></li>
                    @endauth



                        <li class="nav-item"><a href="{{ url('/aboutus')}}" data-scroll-nav="3"> من نحن</a></li>
                        <li class="nav-item"><a href="{{ url('/contact') }}" data-scroll-nav="4">اتصل بنا </a></li>
                    @guest

                        <li class="nav-item"><a href="{{ route('login') }}" data-scroll-nav="7">تسجيل الدخول</a></li>

                        <li class="nav-item"><a href="{{ route('register') }}" data-scroll-nav="6"> تسجيل جديد</a></li>
                    @endguest

                        @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    تسجيل خروج
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endauth
                </ul>

            </div>
        </div>
    </nav>
</header>
    
     

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
