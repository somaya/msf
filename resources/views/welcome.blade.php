
@extends('layouts.app')

@section('content')
    <section class="start_home demo2">
        <div class="banner_top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 start-home-content">
                        <h1>التصلب اللويحي </h1>
                        <p>
                            التصلب اللويحي مرض ناتج عن خلل مناعي يصيب الجهاز العصبي المركزي ويؤثر على الدماغ والحبل
                            الشوكي، يؤدي الى مهاجمة الجسم للمادة البيضاء بالتالي يسبب تلفًا في الغشاء المحيط بالخلايا
                            العصبية والذي يدعى المايلين؛ مما يؤدي إلى تصلب في الخلايا وبالتالي بطء أو توقف سير السيالات
                            العصبية المتنقلة بين الدماغ وأعضاء الجسم.

                        </p>
                        <div class="app-button">
                            <div class="apple-button">
                                <a href="#">
                                    <div class="slider-button-icon">
                                    </div>
                                    <div class="slider-button-title">
                                        <p>البريد الالكتروني</p>
                                        <h3>Info@MSF.com</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="google-button">
                                <a href="#">
                                    <div class="slider-button-icon">
                                    </div>
                                    <div class="slider-button-title">
                                        <p>الجوال</p>
                                        <h3>00966530216596</h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 start-home-img">
                        <img class="img-fluid" src="images/banner-home-2.png" alt="Img" />
                    </div>
                </div>
            </div>
        </div>
        <div class="wave-area">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
    </section>
    @endsection
