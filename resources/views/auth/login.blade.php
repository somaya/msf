
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>عائلة التصلب اللويحي
    </title>

    <link rel="icon" href="favicon.png">
    <!-- Font Icons -->
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Animation -->
    <link rel="stylesheet" href="{{ asset('css/animate.min.cs') }}s">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- Light Case -->
    <link rel="stylesheet" href="{{ asset('css/lightcase.min.css" type="text/css') }}">
    <!-- Template style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="login-page">
    <div class="col-md-4 login-side-des">
        <div class="container-fluid">
            <div class="login-side-block">
                <a href="{{ url('/')}}"><img src="images/MSFLR.png" alt="Logo" /></a>
                <div class="login-reviews">
                    <div class="review-details-content">
                        <div class="owl-carousel review_details" id="review_details-2">
                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>سارة احمد</h5>
                                <h6>مصممة مواقع</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>احمد رائد</h5>
                                <h6>مسوق الكتروني</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>محمود رجب</h5>
                                <h6>مطور مواقع</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>ماركو فليب</h5>
                                <h6>مبرمج</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>لارين متولي</h5>
                                <h6>مصمم مواقع</h6>
                            </div>
                            <!-- End review item -->
                        </div>
                    </div>
                    <div class="review-photo-list">
                        <div class="owl-carousel review_photo" id="review_photo-2">
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-1.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-2.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-3.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-4.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-1.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-1.jpg" alt="IMG">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="container-fluid">
            <a href="index.html" class="res-logo"><img src="images/logo-2.png" alt="Logo" /></a>
            <div class="login-form">

                <div class="login-form-head">
                    <h2>مرحبا بعودتك</h2>
                    <p>تسجيل الدخول لإدارة حسابك.</p>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="signinEmail">البريد الالكتروني</label>
                        <div class="input-group">
                            <div class="input-icon">
                                <span class="ti-email"></span>
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label" for="signinPassword">
                            كلمة المرور
                            <span class="d-flex justify-content-between align-items-center">
                                <a class="link-muted" href="recover-account.html">نسيت كلمة المرور؟</a>
                            </span>
                        </label>
                        <div class="input-group">
                            <div class="input-icon">
                                <span class="ti-lock"></span>
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn theme-btn btn-block">تسجيل الدخول</button>
                    </div>
                    <div class="form-group login-desc">
                        <p> ليس لديك حساب؟ <a href="{{ route('register') }}">مستخدم جديد</a></p>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>


