
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>عائلة التصلب اللويحي
    </title>

    <link rel="icon" href="favicon.png">
    <!-- Font Icons -->
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Animation -->
    <link rel="stylesheet" href="{{ asset('css/animate.min.cs') }}s">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- Light Case -->
    <link rel="stylesheet" href="{{ asset('css/lightcase.min.css" type="text/css') }}">
    <!-- Template style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="login-page">
    <div class="col-md-4 login-side-des">
        <div class="container-fluid">
            <div class="login-side-block">
                <a href="{{ url('/')}}"><img src="images/MSFLR.png" alt="Logo" /></a>
                <div class="login-reviews">
                    <div class="review-details-content">
                        <div class="owl-carousel review_details" id="review_details-2">
                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>سارة احمد</h5>
                                <h6>مصممة مواقع</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>احمد رائد</h5>
                                <h6>مسوق الكتروني</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>محمود رجب</h5>
                                <h6>مطور مواقع</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>ماركو فليب</h5>
                                <h6>مبرمج</h6>
                            </div>
                            <!-- End review item -->

                            <!-- Start review item -->
                            <div class="item">
                                <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود
                                    تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا !"</p>
                                <h5>لارين متولي</h5>
                                <h6>مصمم مواقع</h6>
                            </div>
                            <!-- End review item -->
                        </div>
                    </div>
                    <div class="review-photo-list">
                        <div class="owl-carousel review_photo" id="review_photo-2">
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-1.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-2.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-3.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-4.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-1.jpg" alt="IMG">
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_photo_block">
                                    <img src="images/blog/author-1.jpg" alt="IMG">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="container-fluid">
            <a href="index.html" class="res-logo"><img src="images/logo-2.png" alt="Logo" /></a>
            <div class="login-form">
                <div class="login-form-head">
                    <h2>مرحبا بك في موقعنا</h2>
                    <p>املأ النموذج للبدء بأستخدام الموقع..</p>
                </div>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="username">اسم المستخدم </label>
                        <div class="input-group">
                            <div class="input-icon">
                                <span class="ti-user"></span>
                            </div>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="signinEmail">البريد الالكتروني</label>
                        <div class="input-group">
                            <div class="input-icon">
                                <span class="ti-email"></span>
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label" for="signinPassword">
                            كلمة المرور
                        </label>
                        <div class="input-group">
                            <div class="input-icon">
                                <span class="ti-lock"></span>
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="signinPassword2"> اعادة كلمة المرور</label>
                        <div class="input-group">
                            <div class="input-icon">
                                <span class="ti-lock"></span>
                            </div>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                        </div>
                    </div>
                    <div class="form-group">
                        <p class="checkboxes">
                            <input id="check-er" type="checkbox" name="check">
                            <label for="check-er">انا اوافق على جميع <a href="#"> الشروط و الأحكام</a></label>
                        </p>
                    </div>
                    <div class="form-group">
                        <button class="btn theme-btn btn-block">ابدأ الآن</button>
                    </div>
                    <div class="form-group login-desc">
                        <p> هل لديك حساب بالفعل؟ <a href="signin.html">سجل دخول</a></p>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>




