@extends('layouts.app')

@section('content')


         <div class="page-header">
        <div class="container">
            <div class="title-box">
                <h1 class="title">من نحن </h1>
                <div class="breadcrumb">
                    <span>
                        <a title="Homepage" href="/"><i class="ti ti-home"></i>&nbsp;&nbsp;الرئيسية</a>
                    </span>
                    <span class="bread-sep">&nbsp; | &nbsp;</span>
                    <span> من نحن</span>
                </div>
            </div>
        </div>

    </div>


    <!-- Start How it works Section -->
    <section id="how-it-work" class="section-block" data-scroll-index="2">
        <div class="container">
            <div class="section-header">
                <h2>من نحن </h2>
                <p>
                    طالبات في جامعة الامام عبدالرحمن بن فيصل تخصص نظم المعلومات الادارية، قمنا بدراسة هذا المشروع لتقديم
                    المساندة لأصحاب مرض التصلب اللويحي ومشاركتهم لحظاتهم الحُلوة والمُّره، والوقوف بجانبهم لاجتياز اصعب
                    الازمات مع بعضهم لبعض.
                </p>
            </div>
            >
        </div>
    </section>
    <!-- End How it works Section -->
    @endsection




