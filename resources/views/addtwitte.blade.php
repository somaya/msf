@extends('layouts.app')

@section('content')


<div class="page-header">
        <div class="container">
            <div class="title-box">
                <h1 class="title">المنشورات </h1>
                <div class="breadcrumb">
                    <span>
                        <a title="Homepage" href="/"><i class="ti ti-home"></i>&nbsp;&nbsp;الرئيسية</a>
                    </span>

                    <span class="bread-sep">&nbsp; | &nbsp;</span>
                    <span> اضافة منشور</span>
                </div>
            </div>
        </div>

    </div>

    <section class="section-block reviews-page">
        <div class="container">

            <div class="row">

                <div class="col-md-12">
                    <div class="review-item">
                        <div class="review-head">
                            <div class="review-item-img">
                                <img src="/images/author.jpeg" class="img-fluid" alt="IMG" />
                            </div>
                            <div class="review-author">
                                <h5>  {{ Auth::user()->name }}</h5>
                                
                            </div>
                        </div>

                        <div class="post-comment">
                            <form class="contact-form" method="POST" action="{{ url('/addtwitte') }}">
                                    @CSRF
                                <textarea class="form-control" placeholder="المنشور" name="twitte"></textarea>
                                <button class="btn theme-btn">انشر </button>
                            </form>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>


@endsection
