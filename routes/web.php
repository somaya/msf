<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/aboutus', function () {
    return view('aboutus');
});


Route::get('/contact', function () {
    return view('contact');
});


Route::get('/doctors', function () {
    return view('doctors');
});

Route::get('/sugcom', function () {
    return view('sugcom');
})->middleware('auth');
Route::post('/sugcom', [App\Http\Controllers\MassegeController::class, 'create']);
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/all-twitts', [App\Http\Controllers\HomeController::class, 'allTwitts']);
Route::get('/my-twitts', [App\Http\Controllers\HomeController::class, 'myTwitts'])->middleware('auth');
Route::get('/addtwitte', [App\Http\Controllers\HomeController::class, 'addtwitte'])->name('addtwitte')->middleware('auth');
Route::post('/addtwitte', [App\Http\Controllers\HomeController::class, 'createtwitte'])->name('createtwitte')->middleware('auth');
Route::get('/twitt/{id}/delete', [App\Http\Controllers\HomeController::class, 'delete'])->middleware('auth');
Route::get('/twitt/{id}/edit', [App\Http\Controllers\HomeController::class, 'edit'])->middleware('auth');
Route::post('/twitt/{id}/update', [App\Http\Controllers\HomeController::class, 'update'])->middleware('auth');
